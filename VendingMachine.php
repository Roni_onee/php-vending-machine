<?php
class VendingMachine {
    private $currency;
    private $balance = 0.00; // Use floating-point for balance
    private $drinks = [];

    public function initialize() {
        echo "Въведете код на валута (напр: USD): ";
        $this->currency = trim(fgets(STDIN));
        $this->drinks = $this->getDrinksFromUser();
    }

    private function getDrinksFromUser() {
        $drinks = [];
        echo "Въведете напитките и техните цени (например., 'Coke 1.50', 'Pepsi 1.25') всяка напитка трябва да е на нов ред . Напишете 'done' на нов ред, за да приключите въвеждането на напитки:\n";

        while (true) {
            $input = trim(fgets(STDIN));

            if ($input === 'done') {
                break;
            }

            $parts = explode(' ', $input);
            if (count($parts) === 2 && is_numeric($parts[1])) {
                $drinks[$parts[0]] = (float)$parts[1];
            } else {
                echo "Грешка. Моля въведете валиден продукт с цена(например: pepsi 1.25). Ако иксате да приключите с въвеждането напишете 'done'.\n";
            }
        }

        return $drinks;
    }

    public function orderDrinks() {
        while (true) {
            echo "\n";
            $this->viewDrinks();
            echo "Баланс по вашата сметка: " . number_format($this->balance, 2) . " {$this->currency}\n";

            echo "Въведете 'buy' последвано от разстояния и името на напитката която искате да поръчате (например: 'buy Coke'), напишете 'add' и натиснете Enter за да добавите още средства в сметкат си , напишете 'change' за да вземете рестото си , напишете 'exit' за изход: ";

            $input = trim(fgets(STDIN));

            if ($input === 'exit') {
                break;
            } elseif ($input === 'add') {
                $this->insertCoins();
            } elseif ($input === 'change') {
                $this->getChange();
            } else {
                $parts = explode(' ', $input);
                if (count($parts) === 2 && $parts[0] === 'buy') {
                    $this->buyDrink($parts[1]);
                } else {
                    echo "Невалидна команда. Моля въведете валидна команда.\n";
                }
            }
        }
    }

    private function insertCoins() {
        while (true) {
            echo "Моля поставете вашите монети  (0.05, 0.10, 0.20, 0.50, 1.00) {$this->currency}, когато сте готови напишете 'done' за да депозирате монетите: ";
            $input = trim(fgets(STDIN));
    
            if ($input === 'done') {
                break;
            }
    
            $coin = (float)$input;
    
            if (in_array($coin, [0.05, 0.10, 0.20, 0.50, 1.00], true)) {
                $this->balance += $coin;
                echo "Успешно поставихте" . number_format($coin, 2) . " {$this->currency}. Баланс по вашата сметка: " . number_format($this->balance, 2) . " {$this->currency}\n";
            } else {
                echo "Автомата приема само монети от: 0.05, 0.10, 0.20, 0.50, 1.00 {$this->currency}.\n";
            }
        }
    }
    

    private function getChange() {
        $changeAmount = $this->balance;
        echo "Получихте ресто: " . number_format($changeAmount, 2) . " {$this->currency}\n";
    
        // Calculate and display the specific coins in change
        $denominations = [1.00, 0.50, 0.20, 0.10, 0.05];
        $change = [];
    
        foreach ($denominations as $denomination) {
            $coinCount = floor($changeAmount / $denomination);
            if ($coinCount > 0) {
                $change[] = number_format($denomination, 2) . " {$this->currency} x $coinCount";
                $changeAmount -= $coinCount * $denomination;
            }
        }
    
        // Handle remaining changeAmount if it's not exactly 0.05
        if ($changeAmount > 0) {
            $change[] = number_format($changeAmount, 2) . " {$this->currency} x 1";
        }
    
        if (!empty($change)) {
            echo "в монети:\n";
            foreach ($change as $coin) {
                echo $coin . "\n";
            }
        }
    
        $this->balance = 0.00;
    }
    
    

    public function buyDrink($drinkName) {
        if (isset($this->drinks[$drinkName])) {
            $drinkPrice = $this->drinks[$drinkName];
            if ($this->balance >= $drinkPrice) {
                $this->balance -= $drinkPrice;
                echo "Насладете се на $drinkName! Баланс: " . number_format($this->balance, 2) . " {$this->currency}\n";
            } else {
                echo "Недостатъчна наличност в сметката. Може да добавите още средва като напишете 'add' и натиснете Enter.\n";
            }
        } else {
            echo "Искания продукт не е намерен .\n";
        }
    }

    public function viewDrinks() {
        echo "Налични напитки:\n";
        foreach ($this->drinks as $drink => $price) {
            echo "$drink - " . number_format($price, 2) . " {$this->currency}\n";
        }
    }
}

$vendingMachine = new VendingMachine();

echo "Добре дошли!\n";
$vendingMachine->initialize();
$vendingMachine->orderDrinks();
?>
